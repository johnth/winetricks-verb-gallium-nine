# A [winetricks] verb to enable [gallium nine]

# Usage

```bash
export WINEPREFIX=/tmp/wineenv
winetricks wine_nine.verb wine_nine=off
winetricks --verbose wine_nine.verb wine_nine=vanilla
winetricks --really-verbose wine_nine.verb wine_nine=staging
```

## Check registry status

```bash
export WINEPREFIX=/tmp/wineenv
wine64 regedit /E /tmp/export.reg "HKEY_CURRENT_USER\Software\Wine\DllOverrides"
cat /tmp/export.reg; rm /tmp/export.reg
wine64 regedit /E /tmp/export.reg "HKEY_CURRENT_USER\Software\Wine\DllRedirects"
cat /tmp/export.reg; rm /tmp/export.reg
```

# License: LGPL-2.1

# Source

Based on registry and symlink settings from [ninewinecfg]

[//]: # (Sources)

[winetricks]: https://github.com/Winetricks/winetricks

[gallium nine]: https://github.com/iXit/wine

[ninewinecfg]: https://github.com/iXit/wine/blob/master/programs/ninewinecfg/main.c
